# Library Dependency repository #

This repository contains library dependency to be used in the products who have access to the bitbucket repository.

## Usage
Follow the steps given below to implement the code.



### Environment variables
Add Bitbucket username & password to the System environment variables.
```properties
BITBUCKET_USERNAME=<username>
BITBUCKET_PASSWORD=<password>
```

### Updating Gradle files
Add the below lines in the project level build.gradle file
```groovy
allprojects {
    repositories {
        maven {
            url "https://bitbucket.org/mobiotics/androiddependencycoreimpl/raw/master/"
            credentials {
                username System.getenv("BITBUCKET_USERNAME")
                password System.getenv("BITBUCKET_PASSWORD")
            }
            authentication {
                basic(BasicAuthentication)
            }
        }
    }
}
```

Add the following code in the app level gradle file (app/build.gragle)
```gradle
apply plugin: 'kotlin-kapt'

android {

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
    
    // Add the below lines if gradle sync failed because of multiple 'META-INF/app_release.kotlin_module' files
    packagingOptions{
        exclude 'META-INF/app_release.kotlin_module'
    }
}

dependencies {

    // Base Arch Impl
    implementation 'org.mobiotics.arc:arc:<Version>' //[AndroidAppTemplate](https://bitbucket.org/mobiotics/androidapptemplate)
    
    // Base Arch Impl for Leabback
    implementation 'org.mobiotics.arc:arc-leanback:<Version>' //[AndroidAppTemplate](https://bitbucket.org/mobiotics/androidapptemplate)
    
    implementation 'org.mobiotics:util-kotlin:<Version>' //[AndroidCoreUtilCommonImpl](https://bitbucket.org/mobiotics/androidcoreutilcommonimpl)
    
    // Old-version
    implementation 'org.mobiotics:api-core:<Version>' //[AndroidAPICoreCommonImpl](https://bitbucket.org/mobiotics/androidapicorecommonimpl)
    
    // New Version
    implementation 'org.mobiotics:api:<Version>' //[AndroidAPICoreCommonImpl](https://bitbucket.org/mobiotics/androidapicorecommonimpl)
    
    // New Version with ApiError v1
    implementation 'org.mobiotics:api:<Version>' //[AndroidAPICoreCommonImpl(api-error/v1 branch)](https://bitbucket.org/mobiotics/androidapicorecommonimpl)

    def daggerVersion = '2.21'
    kapt "com.google.dagger:dagger-compiler:$daggerVersion"
}
```

## Versioning

For verisoning of the application,  Read the [README.md](https://bitbucket.org/mobiotics/androiddependencycoreimpl/src/master/org/mobiotics/version/) of the version plugin & update the project.