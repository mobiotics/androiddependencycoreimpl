# APP VERSIONING

To format the version name of the application follow the steps below,



Add the following code in the project level **build.gradle** file

```groovy
buildscript {

    repositories {
        maven {
            url "https://bitbucket.org/mobiotics/androiddependencycoreimpl/raw/master/"
            credentials {
                username System.getenv("BITBUCKET_USERNAME")
                password System.getenv("BITBUCKET_PASSWORD")
            }
            authentication {
                basic(BasicAuthentication)
            }
        }
    }
    dependencies {
        classpath 'org.mobiotics.version:app-version:x.x.x'
    }
}

```



Apply the plugin in top of **app/build.gradle** file 

```groovy
apply plugin: 'app-version'
```



Then do one of the following to apply the version name .

```groovy
android {
    buildTypes {
        debug {
            versionNameSuffix appVersionInfo.debugVersionNameInfo
        }
    }
}
```



Or



Using Flavors

```groovy
android {
  
    flavorDimensions("build")

    productFlavors {
        internal {
            dimension "build"
            versionNameSuffix appVersionInfo.debugVersionNameInfo
        }

        production {
            dimension "build"
            versionNameSuffix appVersionInfo.versionNameInfo
        }
    }
}
```



### Gradle Tasks

After project has synced, two gradle tasks will be added to the project. Run the tasks to print the version name information.

![Gradle task](https://bitbucket.org/mobiotics/androiddependencycoreimpl/raw/772a198e0e8d4f0cf226a2a7765f7d5739a53df7/org/mobiotics/version/screenshots/Screenshot.png)



------



### Versions

* **0.0.2-SNAPSHOT07** - First version



------

Edited using: [Typora](https://typora.io)